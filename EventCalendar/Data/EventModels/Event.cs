﻿using EventCalendar.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventCalendar.Data.EventModels
{
    [Table("Event")]
    public class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }

        [Required]
        public DateTime Datetime { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public EventVisibility Visibility { get; set; }
        
        [Required]
        public long AddressID { get; set; }
        public virtual Address Address { get; set; }

        [Required]
        public int TypeID { get; set; }
        public virtual EventType Type { get; set; }

        public ICollection<EventUser> EventUsers { get; } = new List<EventUser>();
    }
}
