﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventCalendar.Data.EventModels
{
    [Table("EventUser")]
    public class EventUser
    {
        public long EventID { get; set; }
        public Event Event { get; set; }

        public long UserID { get; set; }
        public User User { get; set; }
    }
}
