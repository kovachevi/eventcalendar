﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EventCalendar.Data.EventModels
{
    [Table("UserFriends")]
    public class UserFriends
    {
        // Representation of adjecency list of friends
        [Key, Column(Order = 0)]
        [ForeignKey("FK_UserID_Friend")]
        public long UserID { get; set; }
        public virtual User User { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("FK_User_FriendID")]
        public long FriendID { get; set; }
        public virtual User Friend { get; set; }


    }
}
