﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EventCalendar.Models;
using EventCalendar.Data.EventModels;

namespace EventCalendar.Data
{
    public class EventCalendarContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Event> Event { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserFriends> Friends { get; set; }
        public DbSet<Address> Address { get; set; }

        public EventCalendarContext(DbContextOptions<EventCalendarContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<EventUser>()
                .HasKey(e => new { e.EventID, e.UserID });
            
            builder.Entity<UserFriends>()
                .HasKey(e => new { e.UserID, e.FriendID });

            builder.Entity<UserFriends>()
                .HasOne(fl => fl.User)
                .WithMany(u => u.Friends)
                .HasForeignKey(fl => fl.UserID)
                .OnDelete(DeleteBehavior.Restrict);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
