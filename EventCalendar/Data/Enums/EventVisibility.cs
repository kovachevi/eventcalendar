﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventCalendar.Data.Enums
{
    public enum EventVisibility
    {
        OnlyMe,
        Friends,
        Public
    }
}
