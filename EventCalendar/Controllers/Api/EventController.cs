﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCalendar.Data.EventModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EventCalendar.Services;
using EventCalendar.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EventCalendar.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/event")]
    public class EventController : Controller
    {
        private readonly EventService _service;

        public EventController(EventCalendarContext context)
        {
            _service = new EventService(context);
        }

        // GET: api/events/
        [HttpGet]
        public async Task<IEnumerable<Event>> GetAllAsync()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/event/1
        [HttpGet("{id}", Name = "GetEvent")]
        public async Task<IActionResult> GetEventAsync([FromRoute] long id)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            var calEvent = await _service.GetAsync(id);
            if(calEvent == null)
            {
                return NotFound();
            }
            return Ok(calEvent);
        }

        // PUT: api/event/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync([FromRoute] long id, [FromBody] Event calEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != calEvent.ID)
            {
                return BadRequest();
            }
            else if (!EventExists(id))
            {
                return NotFound();
            }

            try
            {
                await _service.UpdateAsync(calEvent);
            }
            catch (Exception)
            {
                // TODO: Log error
                throw;
            }

            return NoContent();
        }

        // POST: api/event/
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] Event calEvent)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (EventExists(calEvent.ID))
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            
            try
            {
                await _service.AddAsync(calEvent);
            }
            catch (Exception)
            {
                throw;
            }

            return CreatedAtAction("GetEvent", new { id = calEvent.ID }, calEvent);
        }

        // DELETE: api/Event/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var eventExists = EventExists(id);
            if (!eventExists)
            {
                return NotFound();
            }
            Event calEvent = await _service.GetAsync(id);
            await _service.DeleteAsync(calEvent);

            return Ok(calEvent);
        }

        private bool EventExists(long id)
        {
            return _service.EventExists(id);
        }
    }
}
