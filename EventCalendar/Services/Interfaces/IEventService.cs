﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCalendar.Data.EventModels;

namespace EventCalendar.Services.Interfaces
{
    interface IEventService
    {
        Task<IEnumerable<Event>> GetAllAsync();
        Task<Event> GetAsync(long id);
        Task<int> UpdateAsync(Event calEvent);
        Task<int> AddAsync(Event calEvent);
        Task<int> DeleteAsync(Event calEvent);
        bool EventExists(long id);
    }
}
