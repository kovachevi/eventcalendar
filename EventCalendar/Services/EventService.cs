﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCalendar.Data;
using EventCalendar.Data.EventModels;
using EventCalendar.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EventCalendar.Services
{
    public class EventService : IEventService
    {
        private readonly EventCalendarContext _context;

        public EventService(EventCalendarContext context)
        {
            _context = context;
        }

        public async Task<int> AddAsync(Event calEvent)
        {
            _context.Event.Add(calEvent);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(Event calEvent)
        {
            _context.Event.Remove(calEvent);
            return await _context.SaveChangesAsync();
        }

        public async Task<Event> GetAsync(long id)
        {
            return await _context.Event.SingleOrDefaultAsync(e => e.ID == id);
        }

        public async Task<IEnumerable<Event>> GetAllAsync()
        {
            return _context.Event;
        }

        public async Task<int> UpdateAsync(Event calEvent)
        {
            _context.Entry(calEvent).State = EntityState.Modified;
            return await _context.SaveChangesAsync();           
        }

        public bool EventExists(long id)
        {
            return _context.Event.Any(e => e.ID == id);
        }
    }
}
